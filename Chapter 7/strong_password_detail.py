#! python 3
# strong_password_detail.py - Detects whether a password is strong or not. A 
# strong password here is defined as containing at least one digit, both 
# uppercase and lowercase letters, and is at least eight characters in length.
# Compared to strong_password.py, this program presents detailed messages to the
# user explaining what they need to change to make their password stronger.

import re

password = str(input("Please enter a password: "))

# At least eight characters long
password_length_regex = re.compile(r'([\d|\w]){8,}')
# Contains both uppercase and lowercase characters
password_ucase_regex = re.compile(r'([A-Z]+)')
password_lcase_regex = re.compile(r'([a-z])+')
# Has at least one digit
password_number_regex = re.compile(r'[0-9]+')

try:
    length_true = password_length_regex.search(password).group()
except AttributeError:
    print('Password is not at least eight characters')

try:
    u_case = password_ucase_regex.search(password).group()
except AttributeError:
    print('Password does not have at least one uppercase character.')

try:
    l_case = password_lcase_regex.search(password).group()
except AttributeError:
    print('Password does not have at least one lowercase character.')

try:
    num = password_number_regex.search(password).group()
except AttributeError:
    print('Password does not have at least one number.')

else:
    print('Your password is strong!')
