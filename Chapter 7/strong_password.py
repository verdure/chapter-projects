#! python 3
# strong_password.py - Detects whether a password is strong or not. A strong
# password here is defined as containing at least one digit, both uppercase and
# lowercase letters, and is at least eight characters in length.

import re

password = str(input("Please enter a password: "))

strong_pword = re.compile(r'''
                            ^(?=.*?\d)      # Has at least one digit
                            (?=.*?[A-Z])    # Has at least one uppercase letter
                            (?=.*?[a-z])    # Has at least one lowercase letter
                            [A-Za-z\d]{8,}$ # Is at least eight characters long
                            ''', re.VERBOSE)
try:
    detect_strong = strong_pword.search(password).group()
except AttributeError:
    print('Your password is not strong!')
else:
    print('Your password is strong!')
    